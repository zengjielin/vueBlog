import request from "@/utils/request.js";
export function getTags(param){
    return request({
        url:"/api/tags/list",
        method:"get",
        params:param
    });
}