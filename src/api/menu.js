import request from "@/utils/request.js";

/**
 * 获取菜单数据
 */
export function getMenu(){
    return request({
        url:"api/menu",
        method:"get"
    });
}
export function getSelectMenu(){
    return request({
        url:"api/menu/select_menu",
        method:"get"
    });
}

export function changeMenu(params){
    return request({
        url:"api/menu/change",
        method:"post",
        params: {}, 
        data:params
    });
}

/**
 * 获取路由数据
 */
export function getRouter(){
    return request({
        url:"/admin-service/menu/router/admin",
        method:"get"
    });
}

