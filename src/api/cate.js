import request from "@/utils/request.js";

export function getCates(){
    return request({
        url:"/api/cate/list",
        method:"get",
    });
}