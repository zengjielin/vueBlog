import request from "@/utils/request.js";


export function getArticleList(params){
    return request({
        url:"/api/article/list",
        method:"get",
        params:params
    });
}
export function getHotArtices(){
    return request({
        url:"/api/article/hot",
        method:"get"
    });
}

export function getNewArtices(){
    return request({
        url:"/api/article/new",
        method:"get"
    });
}

export function getArchive(){
    return request({
        url:"/api/article/archive",
        method:"get",
    });
}

export function getArticleInfo(params){
    return request({
        url:"/api/article/info",
        method:"get",
        params:params
    });
}
