//import Home from '@/pages/Home'
//import Tags from '@/pages/tags'
//import Category from '@/pages/Category'
//import Archive from '@/pages/Archive'
//import Message from '@/pages/Message'
//import Links from '@/pages/Links'
//import About from '@/pages/About'
//import Article from '@/pages/Article'

const routes = [
    {
      path: '/',
      name: 'Home',
      //component: Home
      component:  resolve => require(['@/pages/Home'], resolve),
    },{
      path: '/tags/:id?',
      name: 'tags',
      //component: Tags
      component:  resolve => require(['@/pages/tags'], resolve),
    },{
      path: '/archives/:year?/:month?',
      name: 'archive',
      //component: Archive
      component:  resolve => require(['@/pages/Archive'], resolve),
    },{
      path: '/message',
      name: 'message',
      //component: Message
      component:  resolve => require(['@/pages/Message'], resolve),
    },{
      path: '/link',
      name: 'link',
      //component: Links
      component:  resolve => require(['@/pages/Links'], resolve),
    },{
      path: '/music',
      name: 'music',
      //component: Links
      component:  resolve => require(['@/pages/Music'], resolve),
    },{
      path: '/about',
      name: 'about',
      //component: About
      component:  resolve => require(['@/pages/About'], resolve),
    },{
      path: '/tool',
      name: 'tool',
      component:  resolve => require(['@/pages/Tool'], resolve),
    },{
      path:'/tag/:id',
      name:'tag',
      //component:Tags
      component:  resolve => require(['@/pages/tags'], resolve),
    },{
      path: '/category/:id?',
      name: 'category',
      //component: Category,
      component:  resolve => require(['@/pages/Category'], resolve),
    },{
      path: '/view/:id',
      name: 'article',
      //component: Article
      component:  resolve => require(['@/pages/Article'], resolve),
    },{
        path:'*',
        redirect:'/'
    }
]

export default routes;
