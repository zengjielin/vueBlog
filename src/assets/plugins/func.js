import $ from 'jquery';
/**
* @desc 获取操作系统类型
* @return {String} 
*/
const getOS = function () {
    var sUserAgent = navigator.userAgent;
    var isWin = (navigator.platform == "Win32") || (navigator.platform == "Windows");
    var isMac = (navigator.platform == "Mac68K") || (navigator.platform == "MacPPC") || (navigator.platform == "Macintosh") || (navigator.platform == "MacIntel");
    if (isMac) return "Mac";
    var isUnix = (navigator.platform == "X11") && !isWin && !isMac;
    if (isUnix) return "Unix";
    var isLinux = (String(navigator.platform).indexOf("Linux") > -1);
    if (isLinux) return "Linux";
    if (isWin) {
        var isWin2K = sUserAgent.indexOf("Windows NT 5.0") > -1 || sUserAgent.indexOf("Windows 2000") > -1;
        if (isWin2K) return "Win2000";
        var isWinXP = sUserAgent.indexOf("Windows NT 5.1") > -1 || sUserAgent.indexOf("Windows XP") > -1;
        if (isWinXP) return "WinXP";
        var isWin2003 = sUserAgent.indexOf("Windows NT 5.2") > -1 || sUserAgent.indexOf("Windows 2003") > -1;
        if (isWin2003) return "Win2003";
        var isWinVista = sUserAgent.indexOf("Windows NT 6.0") > -1 || sUserAgent.indexOf("Windows Vista") > -1;
        if (isWinVista) return "WinVista";
        var isWin7 = sUserAgent.indexOf("Windows NT 6.1") > -1 || sUserAgent.indexOf("Windows 7") > -1;
        if (isWin7) return "Win7";
        var isWin10 = sUserAgent.indexOf("Windows NT 10") > -1 || sUserAgent.indexOf("Windows 10") > -1;
        if (isWin10) return "Win10";
    }
    return "other";
}

/**
  * @desc 获取浏览器类型和版本
  * @return {String} 
  */
const getExplore = function () {
    var sys = {},
        ua = navigator.userAgent.toLowerCase(),
        s;
    (s = ua.match(/rv:([\d.]+)\) like gecko/)) ? sys.ie = s[1] :
        (s = ua.match(/msie ([\d\.]+)/)) ? sys.ie = s[1] :
            (s = ua.match(/edge\/([\d\.]+)/)) ? sys.edge = s[1] :
                (s = ua.match(/firefox\/([\d\.]+)/)) ? sys.firefox = s[1] :
                    (s = ua.match(/(?:opera|opr).([\d\.]+)/)) ? sys.opera = s[1] :
                        (s = ua.match(/chrome\/([\d\.]+)/)) ? sys.chrome = s[1] :
                            (s = ua.match(/version\/([\d\.]+).*safari/)) ? sys.safari = s[1] : 0;

    // 根据关系进行判断
    if (sys.ie) return ('IE ' + sys.ie);
    if (sys.edge) return ('EDGE ' + sys.edge);
    if (sys.firefox) return ('Firefox ' + sys.firefox);
    if (sys.chrome) return ('Chrome ' + sys.chrome);
    if (sys.opera) return ('Opera ' + sys.opera);
    if (sys.safari) return ('Safari ' + sys.safari);
    return 'Unkonwn';
}
const getLang = function () {
    var type = navigator.appName;
    if (type == "Netscape") {
        var lang = navigator.language;//获取浏览器配置语言，支持非IE浏览器
    } else {
        var lang = navigator.userLanguage;//获取浏览器配置语言，支持IE5+ == navigator.systemLanguage
    };
    var lang = lang.substr(0, 2);//获取浏览器配置语言前两位
    return lang
}




/**
 * @desc  设置Cookie
 * @param {String} name 
 * @param {String} value 
 * @param {Number} days 
 */
const setCookie = function (name, value, expires = 3600) {
    var now = new Date();
    var time = now.getTime();
    time += expires * 1000;
    //var _expires = now.toUTCString();
    var _expires = now.setTime(time);
    document.cookie = name + '=' + value + ';expires=' + _expires + ';path=/';
}
/**
* @desc 根据name读取cookie
* @param  {String} name 
* @return {String}
*/
const getCookie = function (name) {
    var prefix = name + "=";
    var cookieStartIndex = document.cookie.indexOf(prefix);
    if (cookieStartIndex == -1) {
        return null;
    }
    var cookieEndIndex = document.cookie.indexOf(";", cookieStartIndex + prefix.length);
    if (cookieEndIndex == -1) {
        cookieEndIndex = document.cookie.length;
    }
    return unescape(document.cookie.substring(cookieStartIndex + prefix.length, cookieEndIndex));
}

/**
 * @desc 根据name删除cookie
 * @param  {String} name 
 */
const removeCookie = function (name) {
    // 设置已过期，系统会立刻删除cookie
    setCookie(name, '1', -1);
}

const formatPassTime = function (startTime) {
    var startTime = Date.parse(startTime);
    var currentTime = Date.parse(new Date()),
        time = currentTime - startTime,
        day = parseInt(time / (1000 * 60 * 60 * 24)),
        hour = parseInt(time / (1000 * 60 * 60)),
        min = parseInt(time / (1000 * 60)),
        month = parseInt(day / 30),
        year = parseInt(month / 12);
    if (year) return year + "年前";
    if (month) return month + "个月前";
    if (day) return day + "天前";
    if (hour) return hour + "小时前";
    if (min) return min + "分钟前";
    else return '刚刚';
}


export default {
    getOS,
    getExplore,
    removeCookie,
    getCookie,
    formatPassTime,
    setCookie,
    getLang
}