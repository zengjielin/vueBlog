// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import VueRouter from 'vue-router'
import ElementUI from 'element-ui';
import VueScrollReveal from 'vue-scroll-reveal';
import 'element-ui/lib/theme-chalk/index.css';
import 'element-ui/lib/theme-chalk/display.css';
import VueI18n from 'vue-i18n'
import routes from './router/index'

//import VueQuillEditor from 'vue-quill-editor'


import 'animate.css'
import './assets/iconfont/iconfont.css'
//import 'font-awesome/css/font-awesome.css'
import './assets/style/style.styl'
import APlayer from '@moefe/vue-aplayer';
import globalFunc from './assets/plugins/func'

import hljs from 'highlight.js' //导入代码高亮文件
import 'highlight.js/styles/monokai-sublime.css'  //导入代码高亮样式
//自定义一个代码高亮指令
Vue.directive('highlight',function (el) {
  let highlight = el.querySelectorAll('pre code');
  highlight.forEach((block)=>{
      hljs.highlightBlock(block)
  })
})

Vue.config.productionTip = false

Vue.use(VueScrollReveal, {
  class: 'scroll-item', // A CSS class applied to elements with the v-scroll-reveal directive; useful for animation overrides.
  duration: 1000,
  scale: 1,
  //distance: '10px',
  mobile: false
});

//富文本编辑器添加实例
//Vue.use(VueQuillEditor, /* { default global options } */)

Vue.use(APlayer,{productionTip: false});
Vue.use(ElementUI);
Vue.use(VueI18n);
Vue.use(VueRouter)

const router =new VueRouter({
  linkActiveClass:'is-active',
	routes,
	mode: 'history'
})

const i18n = new VueI18n({
  locale: 'zh',
  messages:{
    'zh' : require('./lang/zh'),
    'en' : require('./lang/en')
  }
})

// window.onresize = setHtmlFontSize;
// function setHtmlFontSize(){
//     const htmlWidth = document.documentElement.clientWidth || document.body.clientWidth;
//     const htmlDom = document.getElementsByTagName('html')[0];
//     htmlDom.style.fontSize = htmlWidth / 10 + 'px';
// };
// setHtmlFontSize();

// 加入百度统计
router.beforeEach((to, from, next) => {
  if (to.path) {
    if (window._hmt) {
      window._hmt.push(['_trackPageview', '/#' + to.fullPath])
    }
  }

  // 如果用户访问登录页，直接放行
  //if (to.path === '/login') return next()
  // 从sessionStorage中获取保存到的token值
  //const tokenStr = sessionStorage.getItem('token')
  // 没有token跳转到登录页
  //if (!tokenStr) return next('/login')

  next()
})

//当路由跳转页面后返回顶部
router.afterEach((to, from, next) => {
  window.scrollTo(0,0)
})

Vue.prototype.$globalFunc = globalFunc

/* eslint-disable no-new */
new Vue({
	router,
	i18n,
	render: h => h(App),
}).$mount('#app')
