import request from "@/utils/request.js";
export function getMessage(params){
    return request({
        url:"/api/message/list",
        method:"get",
        params:params
    });
}

export function getCommentsByArt(params){
    return request({
        url:"/api/comments/list",
        method:"get",
        params:params
    });
}

export function addMessage(params){
    return request({
        url:"/api/message/add",
        method:"post",
        data:params
    });
}