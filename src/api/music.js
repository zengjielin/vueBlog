import request from "@/utils/request.js";

export function getHotMusic(params){
    return request({
        url:"/music/recommend/songs",
        method:"get",
        params:params
    });
}