import request from "@/utils/request.js";

export function getLinkTop(){
    return request({
        url:"/api/link/top",
        method:"get",
    });
}

export function getLinkList(){
    return request({
        url:"/api/link/list",
        method:"get",
    });
}

export function applyLink(params){
    return request({
        url:"/api/link/apply",
        method:"post",
        data:params
    });
}

