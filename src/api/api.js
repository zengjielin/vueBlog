import request from "@/utils/request.js";


// let base = '/api';

// export const getArticleInfo = params => { return axios.get(`${base}/article/info`, { params: params }); };
// export const getArticleList = params => { return axios.get(`${base}/article/list`, { params: params }); };
// export const getCategory = params => { return axios.get(`${base}/cate/list`, { params: params }); };
// export const getArchiveList = params => { return axios.get(`${base}/article/list`, { params: params }); };
// export const getTagsList = params => { return axios.get(`${base}/tag/list`, { params: params }); };

// export const requestLogin = params => { return axios.post(`${base}/api/login`, params).then(res => res.data); };

// export const getUserList = params => { return axios.get(`${base}/api/user/list`, { params: params }); };

// export const getUserListPage = params => { return axios.get(`${base}/api/user/listpage`, { params: params }); };

// export const removeUser = params => { return axios.get(`${base}/api/user/remove`, { params: params }); };

// export const batchRemoveUser = params => { return axios.get(`${base}/api/user/batchremove`, { params: params }); };

// export const editUser = params => { return axios.get(`${base}/api/user/edit`, { params: params }); };

// export const addUser = params => { return axios.get(`${base}/api/user/add`, { params: params }); };

export function getAbout(){
    return request({
        url:"/api/about/index",
        method:"get",
    });
}
export function getTool(){
    return request({
        url:"/api/tool",
        method:"get",
    });
}

export function record(params){
    return request({
        url:"/api/record",
        method:"post",
        data:params
    });
}